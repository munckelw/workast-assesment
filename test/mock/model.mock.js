class ModelMock {
    constructor(data) {
        Object.assign(this, data);
    }

    save() {
    }

    static findById(params) {
        return new ModelMock({
            userId: "5e73d6e672c00e528428f195",
            title: "Javascript for Dumies",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            tags: [
                "programming",
                "javascript"
            ]
        });
    }

    static deleteOne() {
    }

    static find() {
        return [ new ModelMock({}) ];
    }
}

module.exports = ModelMock;