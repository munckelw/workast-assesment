const mongoose = require('mongoose');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const server = require('./server');

const DB_HOST = process.env.DB_HOST;
const DB_PORT = process.env.DB_PORT;
const DB_NAME = process.env.DB_NAME;

const swaggerOptions = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Workast Assesment API Documentation',
            version: '1.0.0'
        }
    },
    apis: ['./routes/*.js']
};
const swaggerSpec = swaggerJSDoc(swaggerOptions);

mongoose.connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`, { useNewUrlParser: true, useUnifiedTopology: true }).then(
    () => {
        console.log(`Successfully connected to the db`);
        server.init(swaggerSpec, swaggerUI);
    },
    (reason) => {
        console.error(`Error connecting to the db: ${reason}`);
    }
);
