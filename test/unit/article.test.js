const expect = require('expect.js');

const ArticleService = require('../../services/article.service');
const ModelMock = require('../mock/model.mock');

describe('Article Service', () => {
    it('Should create a new article', async () => {
        const data = {
            userId: "5e73d6e672c00e528428f195",
            title: "Javascript for Dumies",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            tags: [
                "programming",
                "javascript"
            ]
        };
        const article = await ArticleService.createArticle(data, ModelMock);
        expect(article.userId).to.eql(data.userId);
        expect(article.title).to.eql(data.title);
        expect(article.text).to.eql(data.text);
        expect(article.tags).to.eql(data.tags);
        expect(article).to.be.a(ModelMock);
    });

    it('Should update an article', async () => {
        const data = {
            title: "Javascript for Dumies Updated",
            text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            tags: [
                "gamming",
            ]
        }
        const article = await ArticleService.updateArticle('5e741895e108315084f463d3', data, ModelMock);
        expect(article).to.not.be(null);
        expect(article.title).to.eql(data.title);
        expect(article.text).to.eql(data.text);
        expect(article.tags).to.eql(data.tags);
        expect(article).to.be.a(ModelMock);
    });

    it('Should delete an article', async () => {
        const deleted = await ArticleService.deleteArticle('5e741895e108315084f463d3', ModelMock);
        expect(deleted).to.be(true);
    });

    it('Should get a list of articles by tag', async () => {
        const articles = await ArticleService.getArticlesByTag(['development'], ModelMock);
        expect(articles).not.to.be(null);
        expect(articles).to.be.an(Array);
        expect(articles.length).to.be.above(0);
        expect(articles[0]).to.be.a(ModelMock);
    });

    it('Should get a list of articles', async () => {
        const articles = await ArticleService.getArticles(ModelMock);
        expect(articles).not.to.be(null);
        expect(articles).to.be.an(Array);
        expect(articles.length).to.be.above(0);
        expect(articles[0]).to.be.a(ModelMock);
    });
});