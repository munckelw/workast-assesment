class BaseService {
    constructor() {
    }

    static async create(data, Model) {
        const entity = new Model(data);
        await entity.save();
        return entity;
    }
};

module.exports = BaseService;