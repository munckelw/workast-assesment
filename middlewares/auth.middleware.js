const _ = require('lodash');

const API_KEY = process.env.API_KEY;

const auth = (req, res, next) => {
    if(req.path.indexOf('/api-docs/') !== -1) {
        next();
    }
    else {
        if(_.isNil(req.headers.authorization) || req.headers.authorization.split(' ')[0] != 'Bearer' || req.headers.authorization.split(' ')[1] != API_KEY) {
            res.sendStatus(401);
        }
        else {
            next();
        }
    }
};

module.exports = auth;