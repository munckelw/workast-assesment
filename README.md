# README

Workast Backend Developer Assesment

### How do I set up the project? ###
 
* Inside the project folder run `npm install` to install all the dependencies

### How do I run the project? ###

  * Inside the project folder run `npm run start`

### How do I run the tests? ###

* Inside the project folder run `npm run test`

### How do I view the API documentation ###

* First you need to run the project
* Open the link [http://localhost:3000/api-docs/](http://localhost:3000/api-docs/)

### How to Authenticate?

* Each request must send an `Authorization` header with the API Key, like: `Autorization: Bearer 5CD4ED173E1C95FE763B753A297D5`

### How to consume the API endpoints ###
* You can do it directly from the documentation page. Please don't forget to first set the API token clicking over the **Authorize** button and entering the following key: `5CD4ED173E1C95FE763B753A297D5`

### Who do I talk to? ###

* Werner Munckel [werner.munckel@gmail.com](mailto:werner.munckel@gmail.com)