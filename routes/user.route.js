const express = require('express');
const router = express.Router();

const UserService = require('../services/user.service');
const User = require('../models/user.model');

/**
 * @swagger
 * tags:
 *  - name: Users
 *    description: User management
 *  - name: Articles
 *    description: Article management
 */

 /**
  * @swagger
  * components:
  *     securitySchemes:
  *         bearerAuth:
  *             type: http
  *             scheme: bearer
  *     responses:
  *         UnauthorizedError:
  *             description: API token is missing or invalid
  * definitions:
  *     NewUser:
  *         type: object
  *         required:
  *             - name
  *             - avatar
  *         properties:
  *             name:
  *                 type: string
  *                 description: Name of the new user
  *             avatar:
  *                 type: string
  *                 description: URL of the avatar to use for the new user
  *     User:
  *         allOf:
  *             - properties:
  *                 _id:
  *                     type: string
  *                     description: The new user ID
  *             - $ref: '#/definitions/NewUser'
  */

/**
 * @swagger
 * 
 * /api/v1/user:
 *  post:
 *      summary: Create a user
 *      description: Add a new user to the app
 *      tags: [Users]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/definitions/NewUser'
 *                  example:
 *                      name: Joe Doe
 *                      avatar: /avatars/image1.jpg
 *      responses:
 *          201:
 *              description: An object with the newly created user's properties
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/definitions/User'
 *                      example:
 *                          _id: 5e73d6e672c00e528428f195
 *                          name: Joe Doe
 *                          avatar: /avatars/image1.jpg
 *          401:
 *              $ref: '#/components/responses/UnauthorizedError'
 *      security:
 *          - bearerAuth: []
 */
router.post('/', async (req, res, next) => {
    try {
        const user = await UserService.createUser(req.body, User);
        res.status(201).send(user);    
    }
    catch(err) {
        console.error(err);
        res.status(err.status || 500).send({msg: err.message});
    }
});

module.exports = router;
