const BaseService = require('./base.service');

class UserService extends BaseService {
    constructor() {
        super();
    }

    static createUser(data, Model) {
        return this.create(data, Model);
    }
}

module.exports = UserService;