const expect = require('expect.js');

const UserService = require('../../services/user.service');
const ModelMock = require('../mock/model.mock');

describe('User Service', () => {
    it('Should create a new user', async () => {
        const data = {
            name: 'Joe Doe',
            avatar: 'avatars/developer.jpg'
        }
        const user = await UserService.createUser(data, ModelMock);
        expect(user.name).to.eql(data.name);
        expect(user.avatar).to.eql(data.avatar);
        expect(user).to.be.a(ModelMock);
    });
});