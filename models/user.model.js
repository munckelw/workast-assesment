const mongoose = require('mongoose');

const schema = mongoose.Schema({
    name: String,
    avatar: String
});

module.exports = mongoose.model("User", schema);
