const _ = require('lodash');

const BaseService = require('./base.service');

class ArticleService extends BaseService {
    constructor() {
        super();
    }

    static createArticle(data, Model) {
        return this.create(data, Model);
    }

    static async updateArticle(id, data, Model) {
        const article = await Model.findById(id);

        if(!_.isNil(article)) {
            if(!_.isNil(data.title)) {
                article.title = data.title;
            }

            if(!_.isNil(data.text)) {
                article.text = data.text;
            }

            if(!_.isNil(data.tags)) {
                article.tags = data.tags;
            }

            article.save();
        }

        return article;
    }

    static async deleteArticle(id, Model) {
        var found;
        var article = await Model.findById(id);

        if(_.isNil(article)) {
            found = false;
        }
        else {
            found = true;
            await Model.deleteOne({ _id: id });
        }
        return found;
    }

    static async getArticlesByTag(tagList, Model) {
        const articles = await Model.find({ tags: { $in: tagList }});
        return articles;
    }

    static async getArticles(Model) {
        const articles = await Model.find();
        return articles;
    }
}

module.exports = ArticleService;