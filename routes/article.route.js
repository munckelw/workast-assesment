const _ = require('lodash');
const express = require('express');
const router = express.Router();

const ArticleService = require('../services/article.service');
const Article = require('../models/article.model');

 /**
  * @swagger
  * 
  * definitions:
  *     NewArticle:
  *         type: object
  *         properties:
  *             userId:
  *                 type: string
  *                 description: The user ID of the author of the article
  *             title:
  *                 type: string
  *                 description: The title of the article
  *             text:
  *                 type: string
  *                 description: The content of the article
  *             tags:
  *                 type: array
  *                 items:
  *                     type: string
  *                 description: The list of tags of the article
  *     Article:
  *         allOf:
  *             - properties:
  *                 _id:
  *                     type: string
  *                     description: The new article ID
  *             - $ref: '#/definitions/NewArticle'
  *     ArticlesList:
  *         type: array
  *         items:
  *             $ref: '#/definitions/Article'
  */

/**
 * @swagger
 * 
 * /api/v1/article:
 *  get:
 *      summary: Get all the Articles
 *      description: Get a list of Articles. Optionally you could send a list of tags to filter only the Articles that match
 *      tags: [Articles]
 *      parameters:
 *          - in: query
 *            name: tag
 *            required: false
 *            schema:
 *              type: array
 *              items:
 *                type: string
 *            description: Optional list of tags to filter by
 *            example: [development, javascript]
 *      responses:
 *          200:
 *              description: A list of Articles
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/definitions/ArticlesList'
 *                      example:
 *                          - _id: 5e741895e108315084f463d3
 *                            userId: 5e73d6e672c00e528428f195
 *                            title: Javascript for Dumies
 *                            text: Lorem ipsum dolor sit amet...
 *                            tags: [development, javascript]
 *          401:
 *              $ref: '#/components/responses/UnauthorizedError'
 *          404:
 *              description: Returned when no Articles were found
 *      security:
 *          - bearerAuth: []
 */
router.get('/', async (req, res, next) => {
    try {
        var articles = [];
        
        if(!_.isNil(req.query.tag)) {
            const tagList = req.query.tag;
            articles = await ArticleService.getArticlesByTag(tagList, Article);
        }
        else {
            articles = await ArticleService.getArticles(Article);
        }
        
        if(articles.length > 0) {
            res.send(articles);
        }
        else {
            res.send(404);
        }
    }
    catch(err) {
        console.error(err);
        res.status(err.status || 500).send({ msg: err.message });
    }
});

/**
 * @swagger
 * 
 * /api/v1/article:
 *  post:
 *      summary: Create an article
 *      description: Add a new article to the app
 *      tags: [Articles]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/definitions/NewArticle'
 *                  example:
 *                      userId: 5e73d6e672c00e528428f195
 *                      title: Javascript for Dumies
 *                      text: Lorem ipsum dolor sit amet...
 *                      tags: [development, javascript]
 *      responses:
 *          201:
 *              description: An object with the newly created article's properties
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/definitions/User'
 *                      example:
 *                          _id: 5e74c4e16308c339ac2ba69d
 *                          userId: 5e73d6e672c00e528428f195
 *                          title: Javascript for Dumies
 *                          text: Lorem ipsum dolor sit amet...
 *                          tags: [development, javascript]
 *          401:
 *              $ref: '#/components/responses/UnauthorizedError'
 *      security:
 *          - bearerAuth: []
 */
router.post('/', async (req, res, next) => {
    try {
        const article = await ArticleService.createArticle(req.body, Article);
        res.status(201).send(article);
    }
    catch(err) {
        console.error(err);
        res.status(err.status || 500).send({ msg: err.message });
    }
});

/**
 * @swagger
 * 
 * /api/v1/article/{id}:
 *  patch:
 *      summary: Updates an article
 *      description: Updates the properties of an article
 *      tags: [Articles]
 *      parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            schema:
 *              type: string
 *            description: The article's ID
 *            example: 5e74c4e16308c339ac2ba69d
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/definitions/NewArticle'
 *                  example: 
 *                      userId: 5e73d6e672c00e528428f195
 *                      title: Javascript for Dumies
 *                      text: Lorem ipsum dolor sit amet...
 *                      tags: [development, javascript]
 *      responses:
 *          200:
 *              description: An object with the updated article's properties
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/definitions/User'
 *                      example:
 *                          _id: 5e74c4e16308c339ac2ba69d
 *                          userId: 5e73d6e672c00e528428f195
 *                          title: Javascript for Dumies
 *                          text: Lorem ipsum dolor sit amet...
 *                          tags: [development, javascript]
 *          401:
 *              $ref: '#/components/responses/UnauthorizedError'
 *      security:
 *          - bearerAuth: []
 */
router.patch('/:id', async (req, res, next) => {
    try {
        const article = await ArticleService.updateArticle(req.params.id, req.body, Article);
        if(_.isNil(article)) {
            res.sendStatus(404);
        }
        else {
            res.send(article);
        }
    }
    catch(err) {
        console.error(err);
        res.status(err.status || 500).send({ msg: err.message });
    }
});

/**
 * @swagger
 * 
 * /api/v1/article/{id}:
 *  delete:
 *      summary: Deletes an article
 *      description: Removes an existing article from the app
 *      tags: [Articles]
 *      parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            schema:
 *              type: string
 *            description: The article's ID
 *            example: 5e74c4e16308c339ac2ba69d
 *      responses:
 *          200:
 *              description: Returned when the article was deleted successfully
 *          401:
 *              $ref: '#/components/responses/UnauthorizedError'
 *          404:
 *              description: Returned when the article was not found
 *      security:
 *          - bearerAuth: []
 */
router.delete('/:id', async (req, res, next) => {
    try {
        const found = await ArticleService.deleteArticle(req.params.id, Article);
        if(found) {
            res.sendStatus(200);
        }
        else {
            res.sendStatus(404);
        }
    }
    catch(err) {
        console.error(err);
        res.status(err.status || 500).send({ msg: err.message })
    }
});

module.exports = router;
