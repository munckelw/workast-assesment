const mongoose = require('mongoose');

const schema = mongoose.Schema({
    userId: mongoose.Types.ObjectId,
    title: String,
    text: String,
    tags: [String]
});

module.exports = mongoose.model("Article", schema);
