const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const auth = require('./middlewares/auth.middleware');
const userRouter = require('./routes/user.route');
const articleRouter = require('./routes/article.route');

const API_PORT = process.env.API_PORT;

const init = (swaggerSpec, swaggerUI) => {
    const app = express();

    app.use(auth);
    app.use(cors());
    app.use(bodyParser.json());

    // Loading routes
    app.use('/api/v1/user', userRouter);
    app.use('/api/v1/article', articleRouter);
    app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpec));

    // HTTP 404 Handler
    app.use((req, res, next) => {
        console.error(`Not Found: ${req.url}`);
        res.status(404).send('Resource not found');
    });

    // Error Handler
    app.use((err, req, res, next) => {
        console.error(`ERROR: ${err.message}`);
        res.status(err.status || 500).send(err.message);
    });

    // Starting HTTP server
    app.listen(API_PORT, () => {
        console.log(`Workast Assesment API listening on port ${API_PORT}`);
    });
}

module.exports.init = init;
